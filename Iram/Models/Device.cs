﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Iram.Models
{
    public class Device
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Идентификационный номер устройства")]
        public Guid DeviceId { get; set; }

        [Display(Name = "Название устройства")]
        public string DeviceName { get; set; }

        [Display(Name ="Время регистрации ")]
        public DateTime RegistrationDate { get; set; }

        [Display(Name = "Последний раз когда заходил пользователь")]
        public DateTime LastEnterDate { get; set; }

        [Display(Name = "Идентификационный номер пользователя")]
        public int UserID { get; set; }

        [Display(Name = "Номер телефона абонента")]
        public string Msisdn { get; set; }

        [Display(Name = "Регистрационный идентификационный номер")]
        public int RegistrationId { get; set; }

        [Display(Name = "Версия")]
        public string Version { get; set; }

        [Display (Name = "Статус")]
        public bool Status { get; set; }
    }
}
