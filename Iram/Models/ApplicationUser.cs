﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Iram.Models
{
    public class ApplicationUser
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Display (Name = "Баланс")]
        public decimal Balance { get; set; }

        [Display (Name = "Последний раз когда пользователь заходил")]
        public DateTime LastEnteredDate { get; set; }

        [Display (Name = "Имя пользователя")]
        public string UserName { get; set; }

        public DateTime RegisteredDate { get; set; }

        public bool Status { get; set; }

        public string UserPhoto { get; set; }

        public bool ReceiveNotification { get; set; }

        [Display (Name ="Номер абонента телефона")]
        public string msisdn { get; set; }

        public string Password { get; set; }
    }
}
