﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Iram.Models
{
    public class SmsFlood
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Display (Name = "Номер телефона абонента")]
        public string Msisdn { get; set; }

        [Display (Name = "Дата отправки")]
        public DateTime Date { get; set; }

        
        public string TrySmsCode { get; set; }
    }
}
