﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
namespace Iram.Models
{
    public class JsonObject
    {
        [JsonPropertyName("Result")]
        public int Result { get; set; }

        [JsonPropertyName("Comment")]
        public int Comment { get; set; }
    }
}
