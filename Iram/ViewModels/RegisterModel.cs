﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Iram.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не правильно указан номер телефона")]
        public string msisdn { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
